# How to configure static IP in **Ubuntu Server**

**nmtui** is not the way for changing or configuring IP in Ubuntu Server 
For this config there is a yaml file which we can change 
```
cd /etc/netplan/ 
```

Or 

```
sudo vim /etc/netplan/00-installer-config.yaml
```
(maybe your file name is different from mine)

```
# This is the network config written by 'subiquity'
network:
  ethernets:
    enp1s0:
      dhcp4: false
      addresses: [192.168.122.14/24]
      gateway4: 192.168.122.1
      nameservers:
        addresses: [8.8.8.8,192.168.122.1]
  version: 2
```
![img](/ubuntu_server_ip/img/ubuntu_ip.png)

![img](/ubuntu_server_ip/img/ubuntu_ip2.png)


