# How to solving Ubuntu 22.04 wifi problem  ? 

- First of all run command to find your wifi module

``` 
lspci
```
- For me it is **RTL8821CE 802.11ac** . 

- Change powersave mode, for this go to **vim /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf** and change line to 2.

```
vim /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf
```

```
[connection]
wifi.powersave = 2
```

- Now install dkms (Dynamic Kernel Module Support (DKMS))
 
```
sudo apt install dkms
```
```
reboot
```

#### Ready 
